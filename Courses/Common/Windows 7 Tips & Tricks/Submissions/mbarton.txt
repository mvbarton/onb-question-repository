(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Lynda
(Course Name): Windows 7: Tips & Tricks
(Course URL): https://www.lynda.com/Windows-7-tutorials/Tips-and-Tricks/79413-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Windows 7 has a program that you can run to get a report of the power efficiency of your laptop.  How do you generate this report?
(A): Run the command powercfg from the command line.
(B): Via the Control Panel
(C): From Microsoft's website
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): True or False: Windows 7 allows you to open a command prompt from any folder.
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): To do this Shift+Right Click on the folder and select "Open command prompt here"
(WF): To do this Shift+Right Click on the folder and select "Open command prompt here"
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): True or False: the keyboard shortcut to open Windows Explorer is Windows+F.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The correct shortcut is Windows+E.
(WF): The correct shortcut is Windows+E.
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 2
(Random answers): 1
(Question): Which  of the following are options available when changing the action of the Power Button in the Start Menu?
(A): Sleep
(B): Log Off
(C): Lock
(D): Restart
(E): Hibernate
(Correct): A,B,C,D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Which of the following items is NOT an option when editing Advanced Folder Options?
(A): Hide empty drives
(B): Hide extensions for known file types
(C): Show drive letters
(D): Show large icons
(Correct): D
(Points): 1
(CF): Icon size configuration can be done for a folder by right-clicking within the folder and choosing View.
(WF): Icon size configuration can be done for a folder by right-clicking within the folder and choosing View.
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a supported way of selecting multiple items from a folder in Windows 7?
(A): CTRL+Click each item you want selected
(B): Click and drag over each item you want selected
(C): Turn on Check boxes for the folder and use the check boxes to select each item
(D): Right Click>>Select on each item you want selected
(e): Use keyboard shortcut CTRL+A if you want to select all of the items within a folder
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Does Windows 7 support previewing files before opening them?
(A): No, this is not supported.
(B): Yes, but only if you have Windows 7 Pro.
(C): Yes, with the Preview Pane, which can be accessed via keyboard shortcut ALT+P.
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): True or False: Windows 7 lets you change the default folder used for Windows Explorer.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What does the Problem Steps Recorder app do?
(A): Takes a flash video of your screen without audio for you to share
(B): Takes a flash video of your screen with audio from your mic for you to share.
(C): Takes a screenshot each second and saves all of them to a MIME HTML file for you to share
(D): Takes a screenshot with each mouse click and saves them to a MIME HTML file for you to share 
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): Why would the Problem Steps Recorder app be useful?
(A): You can send the output to tech support when you have a problem with your computer
(B): You can use the output when filing a bug report for software you are testing
(C): You can turn the output into a tutorial to teach others how to do something.
(D): All of the above.
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 7
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)
