(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): Udemy
(Course Name): Consulting Skills Series - Communication
(Course URL): https://www.udemy.com/consulting-skills-series-communication/#/
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When scheduling a conference call, when should you send the agenda and materials?
(A): Once, on the day of the call
(B): Immediately following the call
(C): During the call
(D): Once prior to the call and one again on the day of the call
(E): Exactly three days prior to the call
(Correct): D
(Points): 1
(CF): It's best to send the agenda and materials both prior to the call and on the day of the call, in case the call participants have lost them.
(WF): It's best to send the agenda and materials both prior to the call and on the day of the call, in case the call participants have lost them.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6 
(Grade style): 0
(Random answers): 1
(Question): What is NOT good advice regarding conference calls?
(A): Put the call on hold if you need to have a side conversation
(B): End on-time or early
(C): Keep introductions brief
(D): Review the agenda
(E): Use the mute button if you have nothing to say at the moment
(Correct): A
(Points): 1
(CF): Putting the call on hold may play music and disrupt the call for the other participants.
(WF): Putting the call on hold may play music and disrupt the call for the other participants.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Intermediate 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is meant by the "parking lot" concept for meetings?
(A): Hold a brief introduction session outside of the meeting room
(B): Keep irrelevant topics in the "parking lot" by discussing them outside of the meeting itself
(C): Follow a fixed seating pattern
(D): Practice your meeting beforehand
(E): Establish a set order in which participants will speak in the meeting
(Correct): B
(Points): 1
(CF): The "parking lot" concept refers to staying on-topic by keeping irrelevant subjects outside of the meeting.
(WF): The "parking lot" concept refers to staying on-topic by keeping irrelevant subjects outside of the meeting.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is NOT good advice for sending e-mail?
(A): Put the word "Invitation" in the subject if inviting the recipient to a meeting or event
(B): Use adjectives and adverbs sparingly
(C): Put the word "Urgent" in the subject if the message needs immediate attention
(D): Always use the BCC (blind copy) feature when sending to multiple recipients
(E): Don't use common texting abbreviations
(Correct): D
(Points): 1
(CF): The blind copy feature should only be used in very specific circumstances.
(WF): The blind copy feature should only be used in very specific circumstances.
(STARTIGNORE)
(Hint):
(Subject): The Consultant's Guide to Email
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): According to the Triune Brain Theory, what type of thinking is the "Lizard" portion of the brain responsible for?
(A): Language
(B): Emotions
(C): Habits
(D): Imagination and creativity
(E): Fight-or-flight response
(Correct): E
(Points): 1
(CF): The "Lizard" portion of the brain handles the fight-or-flight response in humans.
(WF): The "Lizard" portion of the brain handles the fight-or-flight response in humans.
(STARTIGNORE)
(Hint):
(Subject): The Consultant's Guide to People
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): If taking notes during an in-person meeting, what should you use to record them?
(A): Laptop
(B): Tape recorder
(C): Pen and paper
(D): Tablet
(E): Mobile phone
(Correct): C
(Points): 1
(CF): Use pen and paper. It is less disruptive than an electronic device, and meeting participants will know you are paying attention.
(WF): Use pen and paper. It is less disruptive than an electronic device, and meeting participants will know you are paying attention.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is a good piece of advice for creating presentation slides?
(A): Use serif fonts like Georgia or Times New Roman
(B): Make your slides graphic-intensive and use animation frequently to hold viewers' attention
(C): Keep the amount of text to a minimum
(D): Favor clipart over photos
(E): Use many different types of transitions
(Correct): C
(Points): 1
(CF): People read faster than you can speak, so keep text to a minimum on each slide.
(WF): People read faster than you can speak, so keep text to a minimum on each slide.
(STARTIGNORE)
(Hint):
(Subject): Creating Effective Presentations
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When should you do if a key participant is late to a meeting?
(A): Wait for them to arrive before starting the meeting
(B): Start on time and catch up the late attendee afterward
(C): Cancel the meeting
(D): Reschedule the meeting
(E): Stall for time by discussing unimportant matters
(Correct): B
(Points): 1
(CF): It's critical to always start on time, even if an important participant is not there.
(WF): It's critical to always start on time, even if an important participant is not there.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When should the contents of a conference call be considered confidential?
(A): Always
(B): Only if an executive is involved
(C): Never
(D): Only if someone requests it
(E): Only if financial matters are discussed
(Correct): A
(Points): 1
(CF): Always consider the contents of a conference call to be confidential.
(WF): Always consider the contents of a conference call to be confidential.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following is good advice for someone attending a meeting?
(A): Never interrupt someone, even if you disagree
(B): Try to prove your technical superiority
(C): Point your finger when addressing someone
(D): Make an attempt to sell more services to your client if possible
(E): Do not place your hands on the table
(Correct): A
(Points): 1
(CF): You should never interrupt someone who is speaking, even if you disagree with what they are saying.
(WF): You should never interrupt someone who is speaking, even if you disagree with what they are saying.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)