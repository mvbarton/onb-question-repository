(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Derrick Shriver
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who can direct the Development team in how to complete their tasks?
(A): Product Owner
(B): Scrum Master
(C): Both A and B
(D): None of the above
(Correct): D 
(Points): 1
(CF): Within the Scrum Team, the Development Team itself owns the process of completing their tasks.
(WF): Within the Scrum Team, the Development Team itself owns the process of completing their tasks.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Should the first sprint contain the tasks with the highest risk?
(A): Always yes
(B): Usually No
(C): Depends on the budget
(D): Generally yes, but depends on the experience of the Scrum Team
(E): Generally yes, but depends on the experience of the Scrum Team and Business Team
(Correct): E
(Points): 1
(CF): If the team is inexperienced then high-risk tasks can be scheduled after some experience is gained.  Likewise, if the Business Team is inexperienced with Agile, then some time should be given for aclimation to the process.
(WF): If the team is inexperienced then high-risk tasks can be scheduled after some experience is gained.  Likewise, if the Business Team is inexperienced with Agile, then some time should be given for aclimation to the process.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): According to the Scrum Guide, can there be sub-teams within the Development Team?
(A): Yes, because Development teams are self-organizing, but they would not be recognized outside the Development teamn, nor change accountability of the whole team.
(B): No, because Scrum doesn't recognize them
(C): Yes, if decided by the Scrum Master
(D): It depends on management
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which actions best fit the role of Scrum Master?
(A): Directing, Managing, Assessing
(B): Coaching, Teaching, Facilitating
(C): Specifying, Evaluating, Requesting
(D): Prioritizing, Approving, Deciding
(Correct): B
(Points): 1
(CF): Directing, etc.: No one on the Scrum Team does this.  Specifying, etc.: More the Product Owner.  Prioritizing, etc.: Also Product Owner.
(WF): Directing, etc.: No one on the Scrum Team does this.  Specifying, etc.: More the Product Owner.  Prioritizing, etc.: Also Product Owner.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): If a Perscribed Event is skipped (Select all that apply):
(A): This is ok because Agile means this can be decided by the team
(B): This could result in reduced transparency, jeopardizing the opportunity to inspect and adapt
(C): It can be usually remedied by doing two next time
(D): The Scrum Master is accountable
(Correct): B,D
(Points): 1
(CF): The Scrum Master is accountable for Perscribed Events such as Sprint Reviews and Retrospectives.
(WF): The Scrum Master is accountable for Perscribed Events such as Sprint Reviews and Retrospectives.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): The Product Owner is part of the Scrum Team.
(A): True
(B): False
(C): It depends.
(Correct): A
(Points): 1
(CF): The Product Owner is part of the Scrum Team but not the Development Team.
(WF): The Product Owner is part of the Scrum Team but not the Development Team.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
