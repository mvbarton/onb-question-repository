(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): Udemy
(Course Name): Productive Coding with WebStorm
(Course URL): https://www.udemy.com/productive-coding-with-webstorm/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is true about IDE and Text Editor? (choose two)
(A): IDE features error checking, version control, and debugging
(B): Text Editor can be used for lightweight tasks, but can not be used for compiling and debugging 
(C): Files saved from a Text Editor can not be opened in IDE
(D): Text Editor does not have any code format
(Correct): A,B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What are the advantages of Webstorm?
(A): Smart autocomplete
(B): Database navigator
(C): Excellent search engine
(D): Great version control system support
(E): All of the above
(Correct): E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is emmet?
(A): Powerful front-end framework integrated into Webstorm
(B): Useful debugging tool integrated into Webstorm
(C): Webstorm built-in tool for managing different web services
(D): Toolkit to dynamically parse expressions and produce output in order to optimize HTML/CSS workflow
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is the advantage of using live templates?
(A): More productive in coding
(B): Improve code performance
(C): Decrease program bugs
(D): Convenient for code reviews
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What would happen if you drag and drop a css file to a html file in Webstorm?
(A): It will just open up that css file in a new tab
(B): The content of the css file will be appended at the end of the html file
(C): It will automatically create a HTML markup linking to that css file
(D): The content of the css file will be automatically  wrapped in <style> tags and appended to the <head> tag
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): If only the file name is known in the Webstorm workspace, how to let Webstorm to find the path automatically?
(A): Type the name of the file in the href attribute and hit "ctrl space" twice
(B): Type the name of the file in the href attribute and hit "TAB"
(C): Type the name of the file in the href attribute and double click
(D): Type the name of the file in the href attribute and path suggestions would pop up automatically
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is the workflow to place a bookmark in your project? 
(A): Move cursor to the line of interest -> Navigate tab-> Bookmarks -> Toggle bookmark
(B): Move cursor to the line of interest -> Right Click-> Bookmarks -> Toggle bookmark
(C): Move cursor to the line of interest -> Right Click-> Toggle bookmark
(D): Move cursor to the line of interest -> Code tab-> Bookmarks -> Toggle bookmark
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which of the following is integrated with Webstorm?
(A): npm
(B): gulp
(C): grunt
(D): git
(E): All of the above
(Correct): E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is true about Webstorm local history? (choose three)
(A): It is the local version control system that tracks changes to your source code
(B): It enables you to compare versions and roll changes back 
(C): It does not support shared access
(D): It can also track changes in binary files
(Correct): A,B,C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What are unversioned files in Webstorm?
(A): File that are out of date
(B): Files that have not yet been put under version control
(C): Files not tracked by Webstorm local history
(D): Files checked out from a remote branch
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Webstorm
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)