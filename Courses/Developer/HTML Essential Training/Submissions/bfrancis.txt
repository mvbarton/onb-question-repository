(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): HTML Essentials	
(Course URL): http://www.lynda.com/HTML-tutorials/HTML-Essential-Training-2012/99326-2.html
(Discipline): HTML
(ENDIGNORE)

(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What tag would you use to link to a page from your current page? 
(A): <link></link>
(B): <b></b>
(C): <a></a>
(D): <href></href>
(Correct): C
(Points): 1
(CF): Links to pages are created using the anchor tag <a></a>
(WF): Links to pages are created using the anchor tag <a></a>
(STARTIGNORE)
(Hint): 
(Subject): HTML	
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What are the main tags used in a html page? 
(A): html, footer, paragraph
(B): html, head, body
(C): html, header, footer
(D): header, body, footer
(Correct): B
(Points): 1
(CF): The 3 main components of an html page are html, head, body.
(WF): The 3 main components of an html page are html, head, body.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): It is good practice to set a width/height of an image that is its actual size?
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): Correct!
(WF): If you set the width/height to something other than the native size of the image it will likely distort the picture on your page.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): How do you reference an external javascript file to use on your html page? 
(A): Using a script tag and specifying the source file for the script.
(B): Simply place the script file in the same directory as the html and it works.
(C): You cannot externalize javascript, it must be written within the html.
(D): Just add the script file name within the html element on the page.
(Correct): A
(Points): 1
(CF): You reference an external javascript file using the script tag and specifying the source file location.
(WF): You reference an external javascript file using the script tag and specifying the source file location.
(STARTIGNORE)
(Hint): 
(Subject): HTML
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 1
(Random answers): 1
(Question): Which ways could add more than 1 whitespace to text within an html page? (Select all that apply)
(A): ' '
(B): Use a stylesheet
(C): <sp>
(D): &nbsp;
(E): <space>
(Correct): B,D
(Points): 1
(CF): If you want to add more than 1 whitespace to text you need to add &nbsp or use CSS to control this for you.
(WF): If you want to add more than 1 whitespace to text you need to add &nbsp or use CSS to control this for you.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)