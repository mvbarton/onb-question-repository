(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Up and Running With Angular JS
(Course URL): http://www.lynda.com/AngularJS-tutorials/Up-Running-AngularJS/154414-2.html
(Discipline): Angular JS
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Which of the following are valid descriptions of Angular JS? (Select all that apply)
(A): A Framework
(B): Used to generate single page applications
(C): It is not used with HTML
(D): A database is required
(Correct): A, B
(Points): 1
(CF): Angular JS is a JavaScript framework for single page applications.  It is used extensively with HTML, and while you can use one, it does not require a database.
(WF): Angular JS is a JavaScript framework for single page applications.  It is used extensively with HTML, and while you can use one, it does not require a database.
(STARTIGNORE)
(Hint): 
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which is not a feature of Angular JS? 
(A): Directives
(B): Data Binding
(C): Filters
(D): Strongly typed variables
(Correct): D
(Points): 1
(CF): This is JavaScript, it is not a strongly typed language! 
(WF): This is JavaScript, it is not a strongly typed language! 
(STARTIGNORE)
(Hint):
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is one purpose of an Angular View? 
(A): To display the data you have in the Model
(B): To hold data
(C): To update your controller
(D): To hold business logic
(Correct): A
(Points): 1
(CF): Just like normal MVC, the View is shown to the user to display data.
(WF): Just like normal MVC, the View is shown to the user to display data.
(STARTIGNORE)
(Hint): 
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): You should always place the script tag for Angular JS at the bottom of the HTML page.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): While this is true for most JavaScript libraries, Angular will do more than just add basic functionality.  It can take control of your layout.
(WF): While this is true for most JavaScript libraries, Angular will do more than just add basic functionality.  It can take control of your layout.
(STARTIGNORE)
(Hint):
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is a good method in Angular to protect a namespace?
(A): Organize your code into Modules
(B): Keep as few variables as possible
(C): Only keep one View and Model
(D): Only keep one variable per controller
(Correct): A
(Points): 1
(CF): The angular.module() allows you to keep your $scope variable contained and protected in a controller.
(WF): The angular.module() allows you to keep your $scope variable contained and protected in a controller.
(STARTIGNORE)
(Hint):
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): You can only add one filter in a directive.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You can specify multiple filters by using the pipe (|) character.
(WF): You can specify multiple filters by using the pipe (|) character.
(STARTIGNORE)
(Hint): 
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What type of command is this: "ng-controller"?
(A): A Directive
(B): A model assignment
(C): A method call
(D): A filter
(Correct): A
(Points): 1
(CF): Directives start with "ng".  Angular has many different directives. 
(WF): Directives start with "ng".  Angular has many different directives. 
(STARTIGNORE)
(Hint): 
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Why might the following code generate an error in the console: <li ng-repeat="item in names"> <img src="{{item}}.png" /> </li>?
(A): It may look for the image before Angular is loaded
(B): ng-repeat is not a valid directive
(C): You cannot place expressions inside an img tag
(D): there should only be one set of braces in the img src
(Correct): A
(Points): 1
(CF): If Angular isn't loaded yet, the browser will attempt to literally display an image located at "{{item}}.png", which isn't a valid location!  To avoid this, use "ng-src" instead of "src".
(WF): If Angular isn't loaded yet, the browser will attempt to literally display an image located at "{{item}}.png", which isn't a valid location!  To avoid this, use "ng-src" instead of "src".
(STARTIGNORE)
(Hint):
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): You must keep your Angular code and raw JavaScript code completely separate.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Angular IS JavaScript! You can have JavaScript next to Angular and both will function.
(WF): Angular IS JavaScript! You can have JavaScript next to Angular and both will function.
(STARTIGNORE)
(Hint):
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which of the following is needed for deep linking?
(A): $routeProvider
(B): $http
(C): ng-routeProvider
(D): ng-controller
(Correct): A
(Points): 1
(CF): $routeProvider is the service that allows you to set what partials are shown when you navigate to a specified path.
(WF): $routeProvider is the service that allows you to set what partials are shown when you navigate to a specified path.
(STARTIGNORE)
(Hint): 
(Subject): Angular JS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

