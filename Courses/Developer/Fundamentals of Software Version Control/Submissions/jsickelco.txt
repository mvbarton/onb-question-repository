(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Fundamentals of Software Version Control
(Course URL) : http://www.lynda.com/Version-Control-tutorials/Fundamentals-Software-Version-Control/106788-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%2Bsoftware%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Professional 
(ENDIGNORE)

(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What type of version control systems generally allow users to export and e-mail sets of changes from one person to another?
(A): GUI-based
(B): Centralized
(C): Branching
(D): Closed-source
(E): Decentralized
(Correct): E
(Points): 1
(CF): Because they do not depend on a central server, decentralized version control systems usually allow users to export and e-mail sets of changes.
(WF): Because they do not depend on a central server, decentralized version control systems usually allow users to export and e-mail sets of changes.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16 
(Grade style): 0
(Random answers): 1
(Question): Why is a tag or label useful?
(A): It enables you to provide a human-readable label to a set of files and easily revert back to that state
(B): It allows you to provide a reason for a commit, which helps others understand the nature of your changes
(C): It allows you to attach a note to an individual file
(D): It is necessary to branch your code
(E): It is necessary to merge your code
(Correct): A
(Points): 1
(CF): A tag or label allows you to mark the state of a set of files so that it can be easily reverted to.
(WF): A tag or label allows you to mark the state of a set of files so that it can be easily reverted to.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Why is branching a lightweight operation in Git and Mercurial?
(A): Because they are open-source
(B): They have conflict-detection capabilities
(C): They do not actually make a copy of all files to be branched
(D): They allow for continuous integration
(E): They support both forward and reverse integration
(Correct): C
(Points): 1
(CF): Git and Mercurial do not make full file copies when a branch is created.
(WF): Git and Mercurial do not make full file copies when a branch is created.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 1
(Random answers): 1
(Question): What are potential downsides of a centralized version control system?
(A): Centralized version control relies on a single central server, making it more fragile
(B): They do not provide differencing tools
(C): They don't support branching or merging
(D): They lock all files, even text documents
(E): Changes generally cannot be exported and shared via e-mail
(Correct): A, E
(Points): 1
(CF): Centralized version control systems depend on a single server and usually do not allow independent sharing of changes.
(WF): Centralized version control systems depend on a single server and usually do not allow independent sharing of changes.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Which of the following describe reverse integration?
(A): Merging a feature branch to the trunk/main branch
(B): Merging from the main/trunk branch to a feature branch
(C): Reverting back to a prior version
(D): Merging from a remote to local repository
(E): Committing changes to a remote repository
(Correct): A
(Points): 1
(CF): Reverse integration describes merging from a feature branch to the main branch.
(WF): Reverse integration describes merging from a feature branch to the main branch.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What is the difference between a commit and a push?
(A): A commit cannot be undone
(B): A push forces changes to all members of a development team
(C): A commit moves changes from a working set into a local repository, while a push moves changes from a local repository to a remote repository
(D): They are different terms for identical activities
(E): Distributed version control systems use commits while centralized version control systems use pushes
(Correct): C
(Points): 1
(CF): A commit describes changes being checked in locally, while a push moves those changes to a remote repository.
(WF): A commit describes changes being checked in locally, while a push moves those changes to a remote repository.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Where does Git store source control information for files?
(A): In a hidden folder called .git in the same directory as the files themselves
(B): In the /etc directory on Linux and the C:\Windows directory on Windows
(C): It appends source control information to the file itself
(D): In a separate relational database
(E): On the Github server
(Correct): A
(Points): 1
(CF): Source control information is contained in a hidden ".git" directory.
(WF): Source control information is contained in a hidden ".git" directory.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What does the word HEAD mean in this Git command "git checkout HEAD filename.txt"?
(A): It is a reference to the "topmost" change
(B): It is the name of a branch
(C): Is is an argument indicating that only the file header should be checked out
(D): It indicates a forward integration request
(E): It is a reference to the initial version of a file
(Correct): A
(Points): 1
(CF): HEAD indicates the "topmost", or most recent, change.
(WF): HEAD indicates the "topmost", or most recent, change.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Which of the following can help you resolve version control conflicts?
(A): Differencing tools
(B): Perforce
(C): TFS Power Tools
(D): Continuous integration
(E): Shelvesets
(Correct): A
(Points): 1
(CF): Differencing tools help you visualize how two versions of a file differ.
(WF): Differencing tools help you visualize how two versions of a file differ.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 1
(Random answers): 1
(Question): Which of the following describe common version-control operations?
(A): Add
(B): Commit
(C): Compile
(D): Post
(E): Merge
(Correct): A, B, E
(Points): 1
(CF): The included terms associated with version control are Add, Commit, and Merge.
(WF): The included terms associated with version control are Add, Commit, and Merge.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals of Software Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)