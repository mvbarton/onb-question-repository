(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): wthorpe
(Course Site): lynda
(Course Name): Designing a Presentation
(Course URL): http://www.lynda.com/Keynote-tutorials/Designing-Presentation/124082-2.html
(Discipline): professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which of the following tools is not one you would use for working on a presentation?
(A): Drawing tablet
(B): Adobe inDesign
(C): GIMP
(D): Pixelmator
(E): Macromedia flash
(Correct): E
(Points): 1
(CF): Flash embodies many of the things that are considered poor design throughout the course.  All other tools were mentioned during the course
(WF): Flash embodies many of the things that are considered poor design throughout the course.  All other tools were mentioned during the course
(STARTIGNORE)
(Hint):
(Subject): Presentation Tools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): When trying to find out detail of a venue for preparing presentations, what is most likely to be the best resource?
(A): Venue owner
(B): Venue IT worker
(C): Head of the group you are presenting to
(D): The internet
(E): The person who hired or invited you
(Correct): B
(Points): 1
(CF): The IT worker at the venue is most likely to have the exact technical specks for the equipment, such as resolution or room layout.
(WF): The IT worker at the venue is most likely to have the exact technical specks for the equipment, such as resolution or room layout.
(STARTIGNORE)
(Hint):
(Subject): Presentation preparation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What is the best color scheme to work with throughout your presentation?
(A): New colors for every slide, make sure they are complimentary
(B): 4 or 5 related colors that work well together across all slides.
(C): 1 main color and a small number of other colors for emphasis
(D): No more than a dozen colors through the whole slide.
(Correct): B
(Points): 1
(CF): you do not want to overuse a single color, or confuse the audience with too many colors.
(WF): you do not want to overuse a single color, or confuse the audience with too many colors.
(STARTIGNORE)
(Hint):
(Subject): Presentation Layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which of these locations would be a bad place to acquire finished graphics for your presentation?
(A): Pond5.com
(B): Thinkstock.com
(C): Sxc.hu
(D): Google images
(E): Istockphoto.com
(Correct): D
(Points): 1
(CF): Google images are not copyright free, and while convenient, will often be much worse quality as well.
(WF): Google images are not copyright free, and while convenient, will often be much worse quality as well.
(STARTIGNORE)
(Hint):
(Subject): Presentation Layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 2
(Random answers): 1
(Question): Select all of the fonts that would be good choices for normal use in presentations? (check all that apply)
(A): Helvetica
(B): Gotham
(C): League Gothic
(D): Bauhaus
(E): Comic Sans
(Correct): A, B, C
(Points): 1
(CF): Comic Sans and Bauhaus are too specialized for normal use and will feel gimmicky.
(WF): Comic Sans and Bauhaus are too specialized for normal use and will feel gimmicky.
(STARTIGNORE)
(Hint):
(Subject): Presentation layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 1
(Random answers): 1
(Question): What are some ways to save space with photos when building a presentation? (check all that apply)
(A): Change the size of the photo to the size you need and save it as a separate file
(B): Change the format to jpg to save space
(C): Change the format to bmp to save space
(D): Adjust the scaling within the presentation program
(Correct): A, B
(Points): 1
(CF): just changing apparent size wont change the file size.  Bmp is a very large format, jpg is much better suited to minimizing space.
(WF): just changing apparent size wont change the file size.  Bmp is a very large format, jpg is much better suited to minimizing space.
(STARTIGNORE)
(Hint):
(Subject): Presentation layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which of the following is an example of bad typography?
(A): Centering all the text on a slide for consistency
(B): Using contrasting colors, such as white text on a dark background
(C): Using 3-5 bullet points on a slide
(D): Avoiding fonts below 30pt, even when you are presenting larger blocks of information
(Correct): A
(Points): 1
(CF): Too much centering looks out of place and be hard to read.
(WF): Too much centering looks out of place and be hard to read.
(STARTIGNORE)
(Hint):
(Subject): Presentation layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): The "less is more" principle is important because too much information can?
(A): Bore your audience
(B): Give headaches
(C): Force presenter to be 'real'
(D): Sway the focus of the audience
(Correct): C
(Points): 1
(CF): A slide with minimal text forces presenters to speak more naturally rather than reading from the slide.
(WF): A slide with minimal text forces presenters to speak more naturally rather than reading from the slide.
(STARTIGNORE)
(Hint):
(Subject): Presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not an important component of text spacing?
(A): Leading
(B): Kerning
(C): Tracking
(D): Mosaic
(Correct): D
(Points): 1
(CF): Leading is line spacing, Kerning is character spacing, Tracking is group Kerning. Mosaic is a floor tile.
(WF): Leading is line spacing, Kerning is character spacing, Tracking is group Kerning. Mosaic is a floor tile.
(STARTIGNORE)
(Hint):
(Subject): Presentation layout
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which program covered had the widest range of options for data visualization?
(A): Keynote
(B): Adobe Illustrator
(C): MS Powerpoint
(D): Prezi
(Correct): C
(Points): 1
(CF): Powerpoint has vastly more options for types or graph visualizations, as well as integration with excel for more complex datapoints.
(WF): Powerpoint has vastly more options for types or graph visualizations, as well as integration with excel for more complex datapoints.
(STARTIGNORE)
(Hint):
(Subject): Presentation Tools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


